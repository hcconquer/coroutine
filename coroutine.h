#ifndef C_COROUTINE_H
#define C_COROUTINE_H

#define COROUTINE_DEAD 0
#define COROUTINE_READY 1
#define COROUTINE_RUNNING 2
#define COROUTINE_SUSPEND 3

typedef struct schedule schedule_t;

typedef void (*coroutine_func)(schedule_t *, void *ud);

struct schedule *coroutine_open(void);
void coroutine_close(schedule_t *);

int coroutine_new(schedule_t *, coroutine_func, void *ud);
void coroutine_resume(schedule_t *, int id);
int coroutine_status(schedule_t *, int id);
int coroutine_running(schedule_t *);
void coroutine_yield(schedule_t *);

#endif

