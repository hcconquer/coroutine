#include <stdio.h>
#include "coroutine.h"

struct args {
	int num;
};

static void rawdo(schedule_t *schd, void *ud) {
	printf("rawdo begin\n");
	struct args *arg = ud;
	int num = arg->num;
	int i;
	for (i = 0; i < num; i++) {
		printf("coroutine[%d]: %d\n", coroutine_running(schd), i);
		coroutine_yield(schd); // swtich to schd->main
	}
	printf("rawdo end\n");
}

static void test(schedule_t *schd) {
	struct args arg1 = { 3 };
	struct args arg2 = { 5 };
	int co1 = coroutine_new(schd, rawdo, &arg1);
	int co2 = coroutine_new(schd, rawdo, &arg2);
	printf("main begin\n");
	while ((coroutine_status(schd, co1) != COROUTINE_DEAD)
			|| (coroutine_status(schd, co2) != COROUTINE_DEAD)) {
		printf("loop\n");
		coroutine_resume(schd, co1);
		coroutine_resume(schd, co2);
	} 
	printf("main end\n");
}

int main() {
	schedule_t *schd = coroutine_open();
	test(schd);
	coroutine_close(schd);
	
	return 0;
}

