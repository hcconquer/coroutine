#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stddef.h>
#include <string.h>
#include <stdint.h>

#if __APPLE__ && __MACH__
	#include <sys/ucontext.h>
#else 
	#include <ucontext.h>
#endif 

#include "coroutine.h"

#define STACK_SIZE (1024 * 1024)
#define DEFAULT_COROUTINE 16

// coroutine_t;

struct schedule {
	char stack[STACK_SIZE];
	ucontext_t main;
	int nco;
	int cap;
	int running;
	struct coroutine **co;
};

typedef struct coroutine {
	coroutine_func func;
	void *ud;
	ucontext_t ctx;
	struct schedule *sch;
	ptrdiff_t cap;
	ptrdiff_t size;
	int status;
	char *stack;
} co_t;

static co_t *_co_new(schedule_t *schd, coroutine_func func, void *ud) {
	co_t *co = malloc(sizeof(*co));
	co->func = func;
	co->ud = ud;
	co->sch = schd;
	co->cap = 0;
	co->size = 0;
	co->status = COROUTINE_READY;
	co->stack = NULL;
	return co;
}

static void _co_delete(co_t *co) {
	free(co->stack);
	free(co);
}

schedule_t *coroutine_open(void) {
	schedule_t *schd = malloc(sizeof(*schd));
	schd->nco = 0;
	schd->cap = DEFAULT_COROUTINE;
	schd->running = -1;
	schd->co = malloc(sizeof(co_t *) * schd->cap);
	memset(schd->co, 0, sizeof(co_t *) * schd->cap);
	return schd;
}

void coroutine_close(schedule_t *schd) {
	int i;
	for (i = 0; i < schd->cap; i++) {
		co_t *co = schd->co[i];
		if (co) {
			_co_delete(co);
		}
	}
	free(schd->co);
	schd->co = NULL;
	free(schd);
}

int coroutine_new(schedule_t *schd, coroutine_func func, void *ud) {
	co_t *co = _co_new(schd, func, ud);
	if (schd->nco >= schd->cap) { // realloc
		int id = schd->cap;
		schd->co = realloc(schd->co, schd->cap * 2 * sizeof(co_t *));
		memset(schd->co + schd->cap , 0 , sizeof(co_t *) * schd->cap);
		schd->co[schd->cap] = co;
		schd->cap *= 2;
		++schd->nco;
		return id;
	} else {
		int i;
		for (i = 0; i < schd->cap; i++) { // look for a NULL node
			int id = (i + schd->nco) % schd->cap;
			if (schd->co[id] == NULL) {
				schd->co[id] = co;
				++schd->nco;
				return id;
			}
		}
	}
	assert(0);
	return -1;
}

static void _co_func(uint32_t low32, uint32_t hi32) {
	printf("co_func begin\n");
	uintptr_t ptr = (uintptr_t) low32 | ((uintptr_t) hi32 << 32);
	schedule_t *schd = (schedule_t *) ptr;
	int id = schd->running;
	co_t *co = schd->co[id];
	co->func(schd, co->ud);
	_co_delete(co);
	schd->co[id] = NULL;
	--schd->nco;
	schd->running = -1;
	printf("co_func end\n");
}

void coroutine_resume(schedule_t *schd, int id) {
	printf("resume, id: %d\n", id);
	assert(schd->running == -1);
	assert(id >=0 && id < schd->cap);
	co_t *co = schd->co[id];
	if (co == NULL) {
		printf("resume, id: %d, do nothing\n", id);
		return;
	}
	int status = co->status;
	printf("resume, id: %d, status: %d\n", id, status);
	switch(status) {
	case COROUTINE_READY:
		getcontext(&co->ctx);
		co->ctx.uc_stack.ss_sp = schd->stack;
		co->ctx.uc_stack.ss_size = STACK_SIZE;
		co->ctx.uc_link = &schd->main;
		schd->running = id;
		co->status = COROUTINE_RUNNING;
		uintptr_t ptr = (uintptr_t)schd;
		makecontext(&co->ctx, (void (*)(void)) _co_func, 2, (uint32_t) ptr, (uint32_t) (ptr>>32));
		swapcontext(&schd->main, &co->ctx);
		break;
	case COROUTINE_SUSPEND:
		memcpy(schd->stack + STACK_SIZE - co->size, co->stack, co->size);
		schd->running = id;
		co->status = COROUTINE_RUNNING;
		swapcontext(&schd->main, &co->ctx);
		break;
	default:
		assert(0);
	}
}

static void _save_stack(co_t *co, char *top) {
	char dummy = 0;
	ptrdiff_t size = top - &dummy;
	assert(size <= STACK_SIZE);
	if (co->cap < size) {
		free(co->stack); // init is NULL
		co->cap = size;
		co->stack = malloc(co->cap);
	}
	printf("stack size, new: %zd, old: %zd\n", size, co->size);
	co->size = size;
	memcpy(co->stack, &dummy, co->size);
}

void coroutine_yield(schedule_t *schd) {
	int id = schd->running;
	assert(id >= 0);
	co_t *co = schd->co[id];
	assert((char * ) &co > schd->stack);
	_save_stack(co, schd->stack + STACK_SIZE);
	printf("yield, id: %d, status: %d\n", id, co->status);
	co->status = COROUTINE_SUSPEND;
	schd->running = -1;
	swapcontext(&co->ctx, &schd->main);
}

int coroutine_status(schedule_t *schd, int id) {
	assert(id>=0 && id < schd->cap);
	if (schd->co[id] == NULL) {
		return COROUTINE_DEAD;
	}
	return schd->co[id]->status;
}

int coroutine_running(schedule_t *schd) {
	return schd->running;
}

